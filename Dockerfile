FROM alpine:3.8 as base
RUN apk add --no-cache \
  lua lua-dev \
  curl \
  make g++ \
  outils-md5

ENV LUAROCKS_VERSION 3.0.3
RUN cd /tmp && \
  curl -sSL http://luarocks.github.io/luarocks/releases/luarocks-$LUAROCKS_VERSION.tar.gz -o luarocks-$LUAROCKS_VERSION.tgz && \
  tar xzvf luarocks-$LUAROCKS_VERSION.tgz && \
  cd luarocks-$LUAROCKS_VERSION && \
  ./configure && make build && make install && \
  cd /tmp && rm -rf luarocks-$LUAROCKS_VERSION && rm -rf luarocks-$LUAROCKS_VERSION.tgz && cd / && \
  lua -v && \
  luarocks --version

ENTRYPOINT [ "lua" ]
