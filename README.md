# Lua Docker image

| Package  | Version |
| -------- | ------- |
| Lua      | `5.1`   |
| LuaRocks | `3.0.3` |
